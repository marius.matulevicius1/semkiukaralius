﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enterLombardas : MonoBehaviour
{

    public GameObject btobuy;
    public GameObject mainCamera;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                collision.gameObject.transform.position = new Vector3(-89.22f, 158.13f, 0);
                collision.gameObject.transform.localScale = new Vector3(2.38992f, 2.38992f, 2.38992f);
                mainCamera.GetComponent<CameraFollow>().enabled = false;
                mainCamera.transform.position = new Vector3(-76.6f, 167.8f, -10f);
                collision.gameObject.GetComponent<Move>().canMove = false;
                btobuy.SetActive(true);


            }
        }
    }
}
