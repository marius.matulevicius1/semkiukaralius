﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class armor : MonoBehaviour
{

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponent<Animator>().SetBool("hit", true);
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
