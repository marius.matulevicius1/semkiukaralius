﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Teleport : MonoBehaviour
{
    public AudioSource woosh;

    public GameObject woooo;

    public AudioSource city;
    public AudioSource medieval;

    public GameObject Boss;
    public GameObject mainCamera;
    public GameObject drone;
    public GameObject cat;
    public bool canTeleport = false;
    int timesTeleported = 0;
    Animator anim;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        teleport();
    }

    void teleport()
    {
        if (Input.GetKeyDown(KeyCode.T) && canTeleport)
        {
            woosh.Play();
            medieval.Pause();
            GetComponent<Move>().canMove = false;
            anim.SetBool("teleport", true);
        }
    }

    public void zoom()
    {
        if (timesTeleported == 0)
        {
            woooo.GetComponent<VideoPlayer>().Play();
            StartCoroutine(woooo.GetComponent<video>().disabil());

            transform.position = new Vector3(-29.01f, 93.94f, transform.position.z);
            Boss.GetComponent<Boss>().currentBossState = global::Boss.bossStates.playerTeleported;
            timesTeleported = 1;
            enableCat();
            GetComponent<Move>().teleported = true;
            city.Play();
        }
        else
        {
            if (timesTeleported == 1)
            {
                woooo.GetComponent<video>().enabil();
                StartCoroutine(woooo.GetComponent<video>().disabil());

                woooo.GetComponent<VideoPlayer>().Play();
                putInToPlace();
                Boss.GetComponent<Boss>().currentBossState = global::Boss.bossStates.idle;
                transform.localScale = new Vector3(1, 1, 1);
                mainCamera.GetComponent<CameraFollow>().enabled = true;
                timesTeleported = 2;

                GetComponent<Move>().teleported = false;
                GetComponent<Move>().canMove = true;
                medieval.UnPause();
                city.Pause();

            }
        }
    }

    public void enableDrone()
    {
        drone.transform.position = new Vector3(-13.5f + 2, -6.38f + 13, 0);
        drone.SetActive(true);
    }

    public void playWoosh()
    {

    }

    void enableCat()
    {
        cat.GetComponent<Cat>().enabled = true;
    }

    void putInToPlace()
    {
        Boss.transform.position = new Vector3(7.3f, Boss.transform.position.y, 0);
        transform.position = new Vector3(-13.5f, -6.38f, transform.position.z);

    }

}
