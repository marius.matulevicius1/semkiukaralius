﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    Animator anim;
    public float moveSpeed = 10f;
    bool facingRight = true;
    bool moveNow;
    public bool canMove = true;
    public bool teleported = false;

    float maxX1 = 37.8f;
    float maxX2 = -64;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (teleported)
        {
            maxX1 = 53.03f;
            maxX2 = -29.01f;
        }
        else
        {
            maxX1 = 37.8f;
            maxX2 = -64;
        }


        if (canMove)
            Movement();
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.D) && transform.position.x < maxX1)
        {
            anim.SetBool("Move", true);
            if (transform.localScale.x > 0) Flip();
            if (moveNow)
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + moveSpeed * .8f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + moveSpeed * 0.6f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            }
        }
        else
        if (Input.GetKey(KeyCode.A) && transform.position.x > maxX2)
        {
            anim.SetBool("Move", true);
            if (transform.localScale.x < 0) Flip();
            if (moveNow)
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x - moveSpeed * .8f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x - moveSpeed * 0.6f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            }
        }
        else
        {
            anim.SetBool("Move", false);
        }
    }

    public void moveYes()
    {
        moveNow = true;
    }

    public void moveNo()
    {
        moveNow = false;
    }

    void Flip()
    {
        Vector3 Scale = transform.localScale;
        Scale.x *= -1;
        transform.localScale = Scale;
        facingRight = !facingRight;

    }
}
