﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getDamage : MonoBehaviour
{
    public AudioSource getDamageSound;
    public float health = 100;
    public Image healthBar;
    public GameObject deadScreen;
    Animator anim;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void updateHealthBar()
    {
        healthBar.rectTransform.sizeDelta = new Vector2(health, 10);

        if (health <= 0)
        {
            die();
        }
    }

    void die()
    {
        deadScreen.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemyWeapon")
        {
            GetComponent<Move>().canMove = false;
            anim.SetBool("getDamage", true);
            health -= 10;
            updateHealthBar();
        }
    }

    public void playGetDamageSound()
    {
        getDamageSound.Play();
    }
}
