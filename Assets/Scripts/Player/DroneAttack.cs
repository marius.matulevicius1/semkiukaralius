﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAttack : MonoBehaviour
{

    public GameObject Boss;
    public GameObject Player;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.L))
        {
            launchDrone();
        }
        else
        {
            pingpong();

        }
    }

    void pingpong()
    {
        transform.position = new Vector3(Mathf.PingPong(Time.time, 3) + Player.transform.position.x, transform.position.y, transform.position.z);
    }

    void launchDrone()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(Boss.transform.position.x, Boss.transform.position.y + 3), Time.deltaTime * 15);

    }

    public void selfDestruct()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "boss")
        {
            GetComponent<Animator>().SetBool("spin", true);
        }
    }
}
