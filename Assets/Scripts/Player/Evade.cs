﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evade : MonoBehaviour
{
    public GameObject Boss;
    public GameObject BossWeapon;
    Animator anim;
    public bool evadeNow;
    public float jumpBackDistance = 20;
    SpriteRenderer spr;
    // Use this for initialization
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        spr.flipX = evadeNow;
        if (Input.GetKeyDown(KeyCode.S))
        {
            Physics2D.IgnoreCollision(Boss.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(BossWeapon.GetComponent<Collider2D>(), GetComponent<Collider2D>());

            GetComponent<Move>().canMove = false;
            evadeNow = true;
            anim.SetBool("Evade", true);
        }

        if (evadeNow)
        {
            jumpBack();
        }
    }
    void jumpBack()
    {
        if (transform.localScale.x < 0)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + jumpBackDistance, transform.position.y, transform.position.z), Time.deltaTime * .5f);
        }
        if (transform.localScale.x > 0)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x - jumpBackDistance, transform.position.y, transform.position.z), Time.deltaTime * .5f);
        }

    }

    public void unignoreCollision()
    {
        Physics2D.IgnoreCollision(Boss.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);
        Physics2D.IgnoreCollision(BossWeapon.GetComponent<Collider2D>(), GetComponent<Collider2D>(), false);


    }

}
