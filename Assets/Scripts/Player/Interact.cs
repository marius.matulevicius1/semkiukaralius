﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{

    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "rope")
        {
            if (Input.GetKey(KeyCode.E))
            {
                GetComponent<Move>().canMove = false;
                anim.SetBool("interact", true);
                Destroy(collision.gameObject);
            }
        }

        if (collision.gameObject.tag == "armor")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                anim.SetBool("interact", true);
                collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }
}
