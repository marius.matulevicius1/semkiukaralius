﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    float max1 = 28.7f;
    float max2 = -51.9f;

    private void LateUpdate()
    {
        if (target.transform.position.x < max1 && target.transform.position.x > max2)
        {
            Vector3 desiredPosition = target.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;

        }

        if (transform.position.y > 5)
        {
            max1 = 53.10609f;
            max2 = -29.18624f;
        }

    }
}
