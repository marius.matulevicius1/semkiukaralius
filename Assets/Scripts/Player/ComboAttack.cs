﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboAttack : MonoBehaviour
{
    public AudioSource bam;
    public AudioSource combo;
    Animator anim;
    public int kuriComboDaro = 0;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (kuriComboDaro == 0)
            {
                anim.SetBool("Combo1", true);
            }

            if (kuriComboDaro == 1)
            {
                anim.SetBool("Combo2", true);
            }
            if (kuriComboDaro == 2)
            {
                anim.SetBool("Combo3", true);
            }
        }
    }

    public void playBam()
    {
        bam.Play();
    }

    public void playCombo()
    {
        combo.Play();
    }
}
