﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{

    public AudioSource yourDamageIslower;
    public AudioSource getdamage;
    public AudioSource iTried;

    public GameObject PressT;
    public GameObject PressE;
    public GameObject E1;
    public GameObject E2;
    public GameObject E3;
    public GameObject E4;

    public GameObject gameOver;

    public GameObject textBubble;
    public float health = 100;
    public Image healthBar;
    public GameObject Player;
    public bool gotDamage = false;
    bool playerTeleported = false;
    float currentTime = 0f;
    int timesGotDamage = 1;

    public enum bossStates
    {
        walk,
        punch,
        getDamage,
        death,
        gameOver,
        playerTeleported,
        idle,
        firstIdle

    }

    public bossStates currentBossState = bossStates.firstIdle;
    Animator anim;
    public bool moveNow;
    public float moveSpeed = 3f;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (health <= 50)
        {
            PressT.SetActive(true);
            Player.GetComponent<Teleport>().canTeleport = true;
        }


        if (health < 0 || health == 0)
        {
            currentBossState = bossStates.death;

        }
        if (Player.GetComponent<getDamage>().health < 0 || Player.GetComponent<getDamage>().health == 0)
        {
            currentBossState = bossStates.idle;
            anim.SetBool("idle", true);
        }

        if (playerTeleported)
        {
            anim.SetBool("idle", true);
            //currentBossState = bossStates.idle;
        }
        DoStates();
    }

    public void iTriedPlay()
    {
        gameOver.SetActive(true);
        iTried.Play();
        StartCoroutine(talkDamage());
    }

    void timer()
    {
        if (currentTime < 2)
        {
            currentTime += Time.deltaTime;
        }
        if (currentTime > 2)
        {
            changeState();
            currentTime = 0;
        }
    }

    void changeState()
    {
        currentBossState = bossStates.walk;
    }

    void DoStates()
    {
        switch (currentBossState)
        {
            case bossStates.walk:
                walk();
                break;
            case bossStates.punch:
                punch();
                break;
            case bossStates.getDamage:
                getDamage();
                break;
            case bossStates.death:
                death();
                break;
            case bossStates.idle:
                idle();
                break;
        }
    }

    void idle()
    {
        timer();
    }

    void punch()
    {
        anim.SetBool("punch", true);
        currentBossState = bossStates.idle;
    }

    void getDamage()
    {
        anim.SetBool("getDamage", true);
        currentBossState = bossStates.idle;
        updateHealthBar();
    }

    void death()
    {
        anim.SetBool("dead", true);

    }

    public void leBegin()
    {
        StartCoroutine(ifail());
    }

    IEnumerator ifail()
    {
        textBubble.SetActive(true);
        yield return new WaitForSeconds(2f);
        textBubble.SetActive(false);

    }

    void walk()
    {
        anim.SetBool("move", true);
        //zaidejas kairiau
        if (Player.transform.position.x < transform.position.x)
        {
            if (Mathf.Abs(Player.transform.position.x - transform.position.x) < 4f)
            {
                currentBossState = bossStates.punch;
            }

            if (transform.localScale.x < 0)
            {
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }

            if (moveNow)
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x - moveSpeed, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x - moveSpeed * 0.5f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            }
        }

        //zaidejas desiniau
        if (Player.transform.position.x > transform.position.x)
        {
            if (Mathf.Abs(Player.transform.position.x - transform.position.x) < 4f)
            {
                currentBossState = bossStates.punch;
            }

            if (transform.localScale.x > 0)
            {
                var scale = transform.localScale;
                scale = new Vector3(scale.x * -1, scale.y, scale.z);
                transform.localScale = scale;
            }

            if (moveNow)
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + moveSpeed, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + moveSpeed * 0.5f, transform.position.y, transform.position.z), Time.deltaTime * .5f);
            }
        }
    }

    void updateHealthBar()
    {
        healthBar.rectTransform.sizeDelta = new Vector2(health * 2, 10);
    }

    public void moveYes()
    {
        moveNow = true;
    }

    public void moveNo()
    {
        moveNow = false;
    }

    void enableE()
    {
        //PressE.SetActive(true);
        E1.SetActive(true);
        E2.SetActive(true);
        E3.SetActive(true);
        E4.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "playerWeapon")
        {
            if (timesGotDamage <= 3)
            {
                if (timesGotDamage == 3)
                {
                    currentBossState = bossStates.idle;
                    yourDamageIslower.Play();
                    textBubble.SetActive(true);
                    StartCoroutine(talkDamage());
                    timesGotDamage++;
                    enableE();
                }
                else
                {
                    //getdamage.Play();
                    timesGotDamage++;
                    health -= 2;
                }
            }
            gotDamage = true;
            currentBossState = bossStates.getDamage;
            currentTime = 0;
        }

        if (collision.gameObject.tag == "lamp")
        {
            health -= 30;
            gotDamage = true;
            getdamage.Play();
            currentBossState = bossStates.getDamage;
            currentTime = 0;
        }

        if (collision.gameObject.tag == "drone")
        {
            health -= 50;
            gotDamage = true;
            getdamage.Play();
            currentBossState = bossStates.getDamage;
            currentTime = 0;

        }
    }

    IEnumerator talkDamage()
    {
        yield return new WaitForSeconds(3);
        textBubble.SetActive(false);
    }


}
