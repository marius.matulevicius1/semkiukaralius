﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
    public Sprite broken;
    public GameObject Player;
    public GameObject Boss;
    public BoxCollider2D colli;
    public BoxCollider2D trig;

    public AudioSource aa;
    // Use this for initialization
    void Start()
    {
        aa = GetComponent<AudioSource>();
        Physics2D.IgnoreCollision(Player.GetComponent<Collider2D>(), colli);
        Physics2D.IgnoreCollision(Boss.GetComponent<Collider2D>(), colli);
        Physics2D.IgnoreCollision(Player.GetComponent<Collider2D>(), trig);


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            GetComponent<SpriteRenderer>().sprite = broken;
            //Physics2D.IgnoreCollision(Boss.GetComponent<Collider2D>(), trig);
            trig.enabled = false;
            aa.Play();
        }
    }


}
