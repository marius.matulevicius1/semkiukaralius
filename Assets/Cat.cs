﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{

    private void Awake()
    {
        StartCoroutine(selfDestruct());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * 7);
    }

    IEnumerator selfDestruct()
    {
        yield return new WaitForSeconds(200f);
        Destroy(gameObject);
    }
}
