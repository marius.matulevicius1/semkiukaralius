﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdMove : MonoBehaviour
{

    private void Start()
    {
        StartCoroutine(destruction());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * 10, Space.World);
    }

    IEnumerator destruction()
    {
        yield return new WaitForSeconds(30);
        Destroy(gameObject);
    }
}
