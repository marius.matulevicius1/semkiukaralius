﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public GameObject presE;
    public GameObject presT;
    public GameObject drone;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                drone.SetActive(true);
                presT.SetActive(true);
                presE.SetActive(false);
            }
        }
    }
}
