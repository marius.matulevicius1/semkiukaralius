﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class video : MonoBehaviour
{
    public GameObject Player;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator disabil()
    {
        yield return new WaitForSeconds(2.5f);
        GetComponent<VideoPlayer>().enabled = false;
        Player.GetComponent<Animator>().SetBool("appear", true);
    }

    public void enabil()
    {
        GetComponent<VideoPlayer>().enabled = true;
    }
}
