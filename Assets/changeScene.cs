﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class changeScene : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        StartCoroutine(change());
    }

    IEnumerator change()
    {
        yield return new WaitForSeconds(30);
        SceneManager.LoadScene("Tutorial");
    }


}
