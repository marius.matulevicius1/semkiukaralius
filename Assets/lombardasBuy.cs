﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lombardasBuy : MonoBehaviour
{

    public GameObject drone;
    public GameObject mainCamera;
    public AudioSource viiiiisoGero;
    public GameObject bToBuy;
    public GameObject textBubble;


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                Destroy(drone);
                collision.gameObject.GetComponent<Teleport>().enableDrone();
                //Destroy(gameObject);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                viiiiisoGero.Play();
                textBubble.SetActive(true);
                StartCoroutine(exitas(collision));
            }
        }
    }

    IEnumerator exitas(Collider2D collision)
    {
        yield return new WaitForSeconds(1.5f);
        mainCamera.transform.position = new Vector3(53.87999f, 100.23f, -10);
        collision.gameObject.transform.localScale = new Vector3(1, 1, 1);
        mainCamera.GetComponent<CameraFollow>().enabled = true;
        collision.gameObject.GetComponent<Move>().canMove = true;
        collision.gameObject.transform.position = new Vector3(51.11f, 93.87f, 0);
        bToBuy.SetActive(false);
        textBubble.SetActive(false);

    }
}
